let collection = [];

// Write the queue functions below.
// usage of js array methods (push, pop, shift, unshift, etc.) is not allowed
//properties are allowed (like.length)

function print() {
   return collection;
    
}

//add element to rear 
function enqueue(element) {
  collection[collection.length] = element
  return collection;

}

//remove element at front
function dequeue() {


    for(let i = 0; i < collection.length - 1 ; i++){
        collection[i] = collection[i+1]
    }
    return collection;

}

//show element at front
function front() {
    

        return collection[0];

  
    
}

//show total number of elements
function size() {
   return collection.length;
}


//outputs booleans
function isEmpty() {
    if (collection.length === 0 ){
        return true;
    }else{
        return false;
    }
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};